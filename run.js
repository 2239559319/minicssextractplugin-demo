const { webpack } = require('webpack');
const config = require('./webpack.config');

const compiler = webpack(config);

compiler.watch({
  aggregateTimeout: 20,
}, (err, res) => {
  const json = res.toJson().modules;
  console.log(json.length);
});