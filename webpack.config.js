const { join } = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

/**
 * @type {import('webpack').Configuration}
 */
const config = {
  entry: './src/index.js',
  mode: 'development',
  output: {
    path: join(__dirname, 'dist'),
    filename: '[name].js',
  },
  cache: {
    type: 'filesystem',
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        oneOf: [
          {
            use: [MiniCssExtractPlugin.loader, 'css-loader'],
          }
        ]
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin(),
  ]
};

module.exports = config;